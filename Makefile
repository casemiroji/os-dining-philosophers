all: compile

compile:
	@gcc -o main.run *.c  -I . -Wall -O3 -march=native -lpthread
run:
	@./main.run
clean:
	@rm main.run
valgrind:
	valgrind --tool=memcheck --leak-check=yes --show-reachable=yes --num-callers=20 --track-fds=yes --track-origins=yes ./main.run
zip:
	@zip -r entrega.zip *.c *.h Makefile
