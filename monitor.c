#include "monitor.h"

enum {THINKING, HUNGRY, EATING} state[PHILOSOPHERS_COUNT];
pthread_cond_t condition[PHILOSOPHERS_COUNT];
pthread_t philosophers[PHILOSOPHERS_COUNT];
pthread_mutex_t mutex;

int philosophers_sequence_array[PHILOSOPHERS_COUNT];

void print_status(){
    for (int i = 0; i < PHILOSOPHERS_COUNT; i++)
    {
        if (state[i] == HUNGRY)
        {
            printf("{  Philo %d is hungry  }", i);
        }
        else if (state[i] == THINKING)
        {
            printf("{ Philo %d is thinking }", i);
        }
        else if (state[i] == EATING)
        {
            printf("{  Philo %d is eating  }", i);
        }
  
    }
    printf("\n");;
}

void test(int i){
    pthread_mutex_lock(&mutex);
    if ((state[(i + PHILOSOPHERS_COUNT - 1) % PHILOSOPHERS_COUNT] != EATING) && (state[i] == HUNGRY) && (state[(i + 1) % PHILOSOPHERS_COUNT] != EATING)) 
    {
        state[i] = EATING;
        print_status();
        pthread_cond_signal(&condition[i]);
    }
    pthread_mutex_unlock(&mutex);
}

void pickup(int i){

    pthread_mutex_lock(&mutex);
    state[i] = HUNGRY;
    print_status();
    pthread_mutex_unlock(&mutex);

    test(i);
    

    pthread_mutex_lock(&mutex);
    if (state[i] != EATING)
    {
       pthread_cond_wait(&condition[i], &mutex);
    }
    pthread_mutex_unlock(&mutex);
    
}

void putdown(int i){
    pthread_mutex_lock(&mutex);
    state[i] = THINKING;
    pthread_mutex_unlock(&mutex);

    test((i + PHILOSOPHERS_COUNT - 1) % PHILOSOPHERS_COUNT);
    test((i + 1) % PHILOSOPHERS_COUNT);
}

void * eat_and_think(void *arg){
    int *i = (int *) arg;
    pickup(*i);
    // eat
    putdown(*i);
    pthread_exit(0);
}

void initialize_monitor(){
    // inicializa o mutex
    pthread_mutex_init(&mutex, NULL);

    // inicializa as condiões e os estados
    for (int i = 0; i < PHILOSOPHERS_COUNT; i++)
    {
        state[i] = THINKING;
        pthread_cond_init(&condition[i], NULL);
        philosophers_sequence_array[i] = i;
    }
    
    // cria as threads, com seus valores em default

    for (int i = 0; i < PHILOSOPHERS_COUNT; i++)
    {
        pthread_create(&philosophers[i], NULL, eat_and_think, &philosophers_sequence_array[i]);
    }
    
    // finaliza a execucao
    for (int i = 0; i < PHILOSOPHERS_COUNT; i++)
    {
        pthread_join(philosophers[i], NULL);
    }
    
}

void destroy_monitor(){
    // desalloca o mutex
    pthread_mutex_destroy(&mutex);

    // desalloca as conditions
    for (int i = 0; i < PHILOSOPHERS_COUNT; i++)
    {
        pthread_cond_destroy(&condition[i]);
    }
    
}