#ifndef MONITOR_H_
#define MONITOR_H_

#define PHILOSOPHERS_COUNT 5

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

void initialize_monitor();
void destroy_monitor();
void print_status();
#endif